# Shiny application for the visualisation and analysis of receptor and transcriptomic data

### Requirements:

* Linux or Mac OSX

* R (>= v3.5.2)



### To download and run:

1. Open Terminal

2. Clone the project by running: `git clone https://bitbucket.org/kirbyvisp/vdjview.git VDJView` _(you may be prompted for your bitbucket username and password)_

3. Change directory into the newly created VDJView folder by running: `cd VDJView`

4. Open R by running: `R`

5. Install the shiny package by running the R command: `install.packages("shiny")` _(you may be prompted to select a CRAN mirror - choose one appropriate to your location)_

6. Start the shiny app by running the R command: `shiny::runApp("./", launch.browser = TRUE)` _(The first time you run this, you will be prompted to download the appropriate packages, follow the prompts)_



### To view sample data:

1. Click the "Upload data" tab on the left side panel

2. Click the "Browse..." button to specify the receptor data folder and wait for the popup window to load your file system _(this may take a few seconds)_

3. Navigate to the sample_data folder inside the VDJView folder. Once here, click the "Receptor_results" folder. This should result in the "Content" section of the popup showing three .tsv files. Click "Select".

4. Click the "Upload" button in the receptor data folder section. You should now see a venn diagram further down the page with 117 in the "Cells in BCR dataset" section.

5. Click the second "Browse..." button to specify the transcriptomic data folder. Once again, navigate to the sample_data folder, this time selecting the "CuffNorm" folder. You should see three files.

6. Click the second "Upload" button. The venn diagram should now show that the 117 cells are in both the BCR dataset and the gene expression dataset.

7. Additionally, to upload metadata, click the "Browse..." button at the top of the page under the heading "Metadata CSV file (?):". Navigate to the sample_data folder and double click the "metadata.csv" file. This allows you to view any metadata associated with each cell, summarised in the "Cell metadata summary" tab on the left side panel. Uploading metadata also allows the user to filter the uploaded cells, for example, to just look at Naive B cells, we can type `Cell_phenotype = "Naive"` in the text field on the top right of the screen and then click "Filter".

8. From here, you can navigate to the tabs to explore and further analyse the uploaded single cell data.


### FAQs

#### UMAP is not working with error: `could not find function runUMAP`
This is usually caused by the Scater version being out-of-date. Please upgrade to Scater v1.10 at least using the following R commands:

`BiocManager::install() # to update all packages`

`BiocManager::install("scater") # to update just Scater`

Then restart your R session and then you can check your new  Scater version with the following R commands:

`library(Scater)`

`packageVersion("Scater")`

If the problem persists, try installing the `uwot` package with the following R commands: 

`install.packages("uwot")`

`library(uwot)`
  
  
  
#### Uploading transcriptomic data is not working with error: `argument "raw.data" is missing, with no default`
This is usually caused by the Seurat version being out-of-date. Please upgrade to Seurat v4 at least using the following R commands:

`install.packages("Seurat")`

Then restart your R session and then you can check your new  Scater version with the following R commands:

`library(Seurat)`

`packageVersion("Seurat")`
  
  
#

If you encounter any other problems, please contact [Jerome Samir](jsamir@kirby.unsw.edu.au)
